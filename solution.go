package main

import (
	"container/list"
	"context"
	"fmt"
	"github.com/Workiva/go-datastructures/bitarray"
	"io"
	"log"
	"net/http"
	"strconv"
)

type empty interface{}
type semaphore chan empty

type reader struct {
	data         []byte
	bytemask     bitarray.BitArray
	noValidUrls  bool
	howFarAmI    uint64
	initializing bool
}

func (r *reader) Read(p []byte) (n int, err error) {

	log.Println("Reader is being read")
	log.Printf("Bytemask capacity: %d", r.bytemask.Capacity())
	log.Println(r)

	if len(p) == 0 {
		n = 0
		err = nil
		return
	}

	if r.noValidUrls {
		n = 0
		err = fmt.Errorf("no valid urls")
		return
	}

	if len(r.data) == 0 {
		n = 0
		err = io.EOF
		return
	}

	if r.howFarAmI == uint64(len(r.data)) {
		n = 0
		err = io.EOF
		return
	}

	if r.initializing == true {
		n = 0
		err = fmt.Errorf("Still initialising")
		return
	}

	if val, errLocal := r.bytemask.GetBit(r.howFarAmI); r.howFarAmI < uint64(len(r.data)) && errLocal == nil && val == false {
		n = 0
		err = fmt.Errorf("Download in progress")
		return
	}

	/*
		log.Println("Soo bitmask printout:")
		for i := uint64(0); i < r.bytemask.Capacity(); i++ {
			val, _ := r.bytemask.GetBit(i)
			log.Printf("%v", val)
		}
		log.Println("Finished with bitmask printing")
	*/

	var counter int
	for i := 0; i < len(r.data) && i < len(p); i++ {
		bit, errLocal := r.bytemask.GetBit(r.howFarAmI)
		if errLocal != nil {
			log.Println(errLocal)
			break
		}
		if bit {
			p[i] = r.data[i]
			counter++
		} else {
			break
		}
	}
	n = counter
	err = nil
	r.howFarAmI += uint64(n)

	return
}

func DownloadFile(ctx context.Context, urls []string) io.Reader {
	r := &reader{data: make([]byte, 0), bytemask: bitarray.NewBitArray(0), noValidUrls: false, howFarAmI: 0, initializing: true}
	go DownloadFileMaster(ctx, urls, r)
	return r
}

func DownloadFileMaster(ctx context.Context, urls []string, r *reader) io.Reader {

	log.Println("Doing a HEAD for all URLs, to see which servers are active and extract the content length.\n\n")

	var contentLength uint64
	var contentLengthSet bool = false
	var thereWasA200Response = false
	statuses := make(map[string]int)
	client := &http.Client{}
	for _, url := range urls[:] {
		log.Println("\n\n\n")
		log.Println(url)
		resp, err := client.Head(url)
		if err != nil {
			log.Println(err)
			statuses[url] = 0
			continue
		}
		log.Println(resp)

		// Take note of the response status
		statuses[url] = resp.StatusCode

		if resp.StatusCode < 300 && resp.StatusCode > 199 {
			thereWasA200Response = true
		}

		// Get content length
		if !contentLengthSet && resp.StatusCode < 300 && resp.StatusCode > 199 && resp.Header.Get("Content-Length") != "" {
			if s, err := strconv.ParseUint(resp.Header.Get("Content-Length"), 10, 64); err == nil {
				contentLength = s
				contentLengthSet = true
			}
		}
	}

	log.Println("Found the following information follows")
	log.Println("Content Length:")
	log.Println(contentLength)
	log.Println("Servers' statuses:")
	log.Println(statuses)

	// Pointer to data structure
	r.data = make([]byte, contentLength)
	r.bytemask = bitarray.NewBitArray(contentLength)
	r.noValidUrls = false
	r.howFarAmI = 0
	r.initializing = false

	if !thereWasA200Response {
		log.Println("There was no 200 response to HEAD from any server")
		r.noValidUrls = true
		return r
	}

	if !contentLengthSet {
		log.Println("Could not get content length")
		r.noValidUrls = true
		return r
	}

	if contentLength == 0 {
		return r
	}

	log.Println("Initialise needed data structures")

	log.Println(r)

	servers := make([]string, 0, len(urls))
	for k, v := range statuses {
		if v > 199 && v < 300 {
			servers = append(servers, k)
		}
	}

	log.Println(servers)

	//channels := make([]chan, len(servers))
	killers := make([]chan empty, 0, len(servers))
	returns := make([]chan empty, 0, len(servers))
	serverBytes := make(map[int]string)

	// Put which server is responsible for which bytes
	{
		var currentPosition uint64
		currentServer := 0
		serverCount := len(servers)
		l := distributeIntegerBytesOverServers(contentLength, uint64(serverCount))
		e := l.Front()
		for {
			if e.Next() != nil {
				log.Println("Putting range value in bytemap")
				log.Println(e.Value)
				offset, ok := e.Value.(uint64)
				if !ok {
					panic("Internal error")
				}
				serverBytes[currentServer] = strconv.FormatUint(currentPosition, 10) +
					"-" +
					strconv.FormatUint(currentPosition+offset-1, 10)
				currentServer += 1
				currentPosition += offset
				e = e.Next()
			} else {
				break
			}
		}
	}

	log.Println(killers)
	log.Println(serverBytes)

	for i, url := range servers {
		log.Println("Starting routine for server number and url:")
		log.Println(i)
		log.Println(url)
		killers = append(killers, make(chan empty))
		returns = append(returns, make(chan empty))
		go func(r *reader, killChan, returnChan chan empty, serverNum int, serverUrl string, serverBytes *map[int]string, client *http.Client, smph semaphore) {
			req, _ := http.NewRequest("GET", serverUrl, nil)
			req.Header.Add("Range", "bytes="+(*serverBytes)[serverNum])
			resp, err := client.Do(req)

			if err == nil {
				if resp.StatusCode > 199 && resp.StatusCode < 300 {
					var beg, end uint64
					fmt.Sscanf((*serverBytes)[serverNum], "%d-%d", &beg, &end)

					log.Println("Beginning and end:")
					log.Println(beg)
					log.Println(end)

					if uint64(resp.ContentLength) != end-beg+1 {
						panic("Aaaah! Problem!")
					}

					tempp := make([]byte, resp.ContentLength)
					resp.Body.Read(tempp)
					log.Println("tempp")
					log.Println(tempp)

					for i := beg; i <= end; i++ {
						r.bytemask.SetBit(i)
						r.data[i] = tempp[i-beg]
					}

					log.Println("Done thread for " + serverUrl)
				} else {
					panic("Server error!")
				}
			} else {
				// Do something
				log.Println(err)
				panic("Server error!")
			}
		}(r, killers[i], returns[i], i, url, &serverBytes, client, nil)
	}
	return r
}

func distributeIntegerBytesOverServers(bytes uint64, serverCount uint64) (l list.List) {
	l = *(l.Init())

	quotient := bytes / serverCount
	log.Printf("Quotient: %v\n", quotient)

	if bytes%serverCount == 0 {
		for i := uint64(0); i <= serverCount; i++ {
			l.PushBack(quotient)
		}
		return
	}

	currentCount := serverCount
	for bytes > 0 {
		l.PushBack(quotient)
		bytes -= quotient
		currentCount--
		if bytes%currentCount == 0 {
			q := bytes / currentCount
			for i := uint64(0); i <= currentCount; i++ {
				l.PushBack(q)
			}
			return
		}
	}
	return
}

func main() {

	l := distributeIntegerBytesOverServers(200, 3)
	e := l.Front()
	for {
		if e.Next() != nil {
			log.Println(e.Value)
			e = e.Next()
		} else {
			break
		}
	}

	urls := []string{
		"http://mirrors.ordimatic.net/mint/stable/17/sha256sum.txt",
		"http://ftp-stud.hs-esslingen.de/pub/Mirrors/linuxmint.com/stable/17/sha256sum.txt",
		"http://mirror.csclub.uwaterloo.ca/linuxmint/stable/17/sha256sum.txt",
		"http://mirrors.netix.net/LinuxMint/linuxmint-iso/stable/17/sha256sum.txt",
		"http://mirrors.uni-ruse.bg/linuxmint/iso/stable/17/sha256sum.txt",
		"http://ftp.crifo.org/mint-cd/stable/17/sha256sum.txt",
	}

	log.Printf("Starting attempt to download from %v servers\n\n", len(urls))

	var r io.Reader = DownloadFile(nil, urls)

	log.Println(r)
	p := make([]byte, 10000)
	pVerify := make([]byte, 10000)
	num, _ := r.Read(p)
	resp, _ := http.Get("http://mirrors.ordimatic.net/mint/stable/17/sha256sum.txt")
	resp.Body.Read(pVerify)
	log.Println(string(p))

	log.Println("Comparing the two downloaded files")
	var areEqual bool = true
	for i := 0; i < num; i++ {
		if p[i] != pVerify[i] {
			log.Println("DIFFERENT!!!")
			log.Println(i)
			log.Printf("Byte difference: %v should be %v\n", p[i], pVerify[i])
			areEqual = false
		}
	}
	log.Println(p)
	log.Println(pVerify)
	if areEqual {
		log.Println("The two arrays are equal")
	}
}
